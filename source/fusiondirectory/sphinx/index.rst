Write User Documentation
========================

How to properly write a documentation in Sphinx for your new plugin.

.. toctree::
   :maxdepth: 2

   sphinx.rst