Writing documentation within your code
=======================================

How to properly write documentation within your code.

.. toctree::
   :maxdepth: 2

   PHPDoc