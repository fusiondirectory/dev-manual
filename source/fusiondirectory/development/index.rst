Development
===========

.. toctree::
   :maxdepth: 2


   contribute/guidelines.rst
   howtodev/index.rst
   codingstyle/index.rst
   writeplugin/index.rst   
   api/index.rst
   automatedtesting/index.rst
   documentation/index.rst
