## %"FusionDirectory 1.4" - 2024-12-19

### Added

#### dev-manual
- dev-manual#27 Document the rest api
- dev-manual#54 Document fusiondirectory-ldap library
- dev-manual#57 Add documentation about yaml file used to add a simple way to add / delete plugin
- dev-manual#59 PL Section Priority update for Mail Template within Dev Manual
- dev-manual#65 add the labeling workflow into the dev manual
- dev-manual#71 Add documentation for integrator, tools, orchestrator
- dev-manual#73 rewrote the new branch feature page
- dev-manual#75 [User-Manual] 1.4 LDAP Numbering needs updates since FranceConnect new schema

### Changed

#### dev-manual
- dev-manual#33 remove the need for main.inc from the dev manual
- dev-manual#38 change the link for transifex in the manual
- dev-manual#40 rework the manual to remove index with only 1 file
- dev-manual#41 Adapt manual to new Dialogs interface
- dev-manual#43 Access to rest.fusiondirectory.org
- dev-manual#53 the minimal version of php for 1.4 has been changed this has to be reflected into the manual
- dev-manual#62 Updates menu section numbering to include task within FD configuration section
- dev-manual#81 Adapt the manual to the new rules of branch

### Removed

#### dev-manual
- dev-manual#39 remove the proxy-fd.schema from the list of schema in the dev manual
- dev-manual#74 Remove the manpages update section as we no longer have mapages

### Fixed

#### dev-manual
- dev-manual#26 Review the developper manual and adapt it to the change we have made during filters / management reorganization
- dev-manual#68 update the prerequisite for fusiondirectory 1.4

## %"FusionDirectory 1.3.1" - 2023-06-23

### Added

#### dev-manual
- dev-manual#1 adding release policies
- dev-manual#2 adding license
- dev-manual#4 Adding more documentation
- dev-manual#5 adding code of conduct
- dev-manual#6 adding an contributing section to the manual
- dev-manual#15 Add translate Fusiondirectory section
- dev-manual#19 Add schema number for sinaps config schema
- dev-manual#20 add schema number for supann extension
- dev-manual#21 reservation of a OID number  for the schema of fusiondirectory-plugins-seafile and fusiondirectory-plugins-libreroaming
- dev-manual#30 Guidelines for better contributions
- dev-manual#31 code of conduct
- dev-manual#44 add the release workflow of our various software to the the manual
- dev-manual#45 new subsection in ldap schema "FusionDirectory attribute finder"
- dev-manual#49 add support / security / contact us like in the user manual
- dev-manual#51 add what is fusiondirectory / prerequisite /certified distribution

### Changed

#### dev-manual
- dev-manual#3 reorganising logically the documentation
- dev-manual#7 latest version of contributing.md
- dev-manual#18 move how_to_contribute page from the wiki to developper documentation
- dev-manual#17 move how_to_contribute page from the wiki to developper documentation
- dev-manual#25 rewrite the FusionDirectory Life Cycle page
- dev-manual#29 rewrite distribution and php support
- dev-manual#32 Update the contribute part of the Contributing to FusionDirectory
- dev-manual#37 change the old pictures showing fusiondirectory 1.0.8 in the manual
- dev-manual#47 correct the fusiondirectory cycle that is at 6 month when is should be 12 month
- dev-manual#48 move all fusiondirectory dev manual into a fusiondirectory subidrectory

### Removed

#### dev-manual
- dev-manual#50 remove centos 8 from te supported distribution

### Fixed

#### dev-manual
- dev-manual#13 Correct the exemple about php codesniffer
- dev-manual#9 Coding standard examples should match the coding standards
- dev-manual#10 The code sniffer command is wrong
- dev-manual#28 change the url in 1.3 for the webservice documentation
- dev-manual#52 replace freenode by libera
- dev-manual#66 clarify the php version supported for FusionDirectory 1.3.x
- dev-manual#67 update the certified distribution matrix

